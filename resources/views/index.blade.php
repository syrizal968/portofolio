@extends('layout.layout')
@section('conten')
    <div class="container">
        <section id="home" class="mb-5">
            <div class="row align-items-center hu">
                <div class="col-md-6 home-left">
                    <h4 class="text-success">Hai 👋, Saya</h4>
                    <h2 class="text-light p55">SYAIFUL <span class="text-success">RIZAL</span></h2>
                    <P class="text-success">Seorang Developer <span class="text-light">& Mahasiswa Aktif UIM (Universitas
                            Islam
                            Madura)</span></P>
                    <a class="btn btn-success m-1" href="https://gitlab.com/syrizal968" target="_blank"
                        rel="noopener noreferrer">GitLab</a>
                    <a class="btn btn-dark m-2" href="https://github.com/Rizalsfr" target="_blank"
                        rel="noopener noreferrer">GitHub</a>
                </div>
                <div class="col-md-6 home-right text-center">
                    <img src="{{ url('assets/img/r.jpg') }}" width="50%" alt="foto">
                </div>
            </div>
            <div>
                <a class="btn btn-success m-1" href="https://web.facebook.com/syiful.risal" target="_blank"
                    rel="noopener noreferrer">
                    <i class="bi bi-facebook"></i></a>
                <a class="btn btn-dark m-1" href="https://wa.me/6285648729867" target="_blank" rel="noopener noreferrer">
                    <i class="bi bi-whatsapp"></i></i></a>
                <a class="btn btn-success m-1" href="https://www.instagram.com/__sf_r__/" target="_blank"
                    rel="noopener noreferrer">
                    <i class="bi bi-instagram"></i></a>
                <a class="btn btn-dark m-1" href="https://vm.tiktok.com/ZM2cfA6Ye/" target="_blank"
                    rel="noopener noreferrer">
                    <i class="bi bi-tiktok"></i></a>
            </div>
        </section>
    </div>

    <section class="h bc mb-5" id="tentang">
        <div class="container">
            <h1 class="text-center text-success mt-6">Tentang</h1>
        </div>
    </section>
    <section class="h mt-5" id="portofolio">
        <div class="container">
            <div class="d-flex justify-content-center pt-5">
                <ul class="nav nav-underline">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">Active</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" aria-disabled="true">Disabled</a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <section class="hu bc" id="kontak">
        <div class="container">
            <h1 class="text-center text-success mt-6">Kontak</h1>
        </div>
    </section>
@endsection
