<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="{{ asset('assets/img/r.jpg') }}" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">
    <title>syaiful rizal</title>
    <style>
        .h {
            min-height: 100vh;
        }

        .hu {
            min-height: 90vh;
        }

        .p55 {
            font-size: 50px;
            font-weight: bold;
        }

        .bc {
            background-color: #1F252D;
        }

        .footer a {
            text-decoration: none;
            color: #28D07E;
        }
    </style>
</head>

<body>
    @include('layout.navbar')
    @yield('conten')
    @include('layout.footer')

    <script src="{{ asset('assets/js/bootstrap.js') }}"></script>
</body>

</html>
