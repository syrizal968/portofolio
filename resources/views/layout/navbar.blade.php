    <nav class="navbar fixed-top navbar-expand-lg  navbar-dark bg-dark">
        <div class="container">
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo03"
                aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="#">Navbar</a>
            <div class="d-flex">
                <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item p-2">
                            <a class="nav-link" aria-current="page" href="#home">Home</a>
                        </li>
                        <li class="nav-item p-2">
                            <a class="nav-link" aria-current="page" href="#tentang">Tentang</a>
                        </li>
                        <li class="nav-item p-2">
                            <a class="nav-link" aria-current="page" href="#portofolio">Portofolio</a>
                        </li>
                        <li class="nav-item p-2">
                            <a class="nav-link" aria-current="page" href="#kontak">Kontak</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
